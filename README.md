# Fingerprinting Benchmarks

Quick & dirt performance testing with real-world xxHash64 and SHA-256 implementations in the browser.

## Discoveries

- Subtle Crypto
  - Con: It is 7-8x slower 😱 than pure JS implementations.
  - There is <10% difference (within margin of error) between SHA-1, SHA-256, and SHA-512 digest algos using Subtle Crypto.
  - Pro: Asynchronous (Promises) so doesn't block the main thread.
  - Pro: Built-in to the browser so less code for clients to download.
- xxHash64's Javascript port is not designed for the browser. Relies on Buffer and `module.export` which must be shimmed. Might as well use the native bindings on Nodejs though.
- js-sha256 and xxHash64 are not much different in performance when processing tiny chunks of data. Probably too much overhead outside of their respective algorithms. Neither are obviously faster or slower with sizes from 50-100 bytes/characters and 100-10,000 iterations.

## Example Outputs

Platform: 2015 Macbook / Chrome 56 (Canary)

```
Generating 50 megabytes of random data
Fingerprinting 100000 x 500 bytes
xxhash64: 433ms
kazuho/js-sha256: 542ms
Subtle: 7477ms
```

```
Generating 1 megabytes of random data
Fingerprinting 10000 x 100 bytes
xxhash64: 113ms
kazuho/js-sha256: 75ms
Subtle: 783ms
```

## Usage

Download the xxHash64 library from NPM:
```
npm install
```

Use a local static webserver to open the benchmark runner:
```
npm install http-server
./node_modules/.bin/http-server -o
```

## Configuration

Change the size and iterations constants to re-run with different metrics.
```js
// random string/buffer length (e.g. typical URL length)
const size = 100

// number of strings/buffers
const iterations = 10000
```

## See Also
- [More Hash Function Tests](http://aras-p.info/blog/2016/08/09/More-Hash-Function-Tests/) - Comparison of fingerprinting algos on various platforms
- [Digest algorithm (httpwg/http-extensions#228)](https://github.com/httpwg/http-extensions/issues/228) - Discussion of alternatives to SHA-256 for reasons including convenience, security, & performance

## Colophon
Benchmarked ⏱ by Sebastiaan Deckers in Singapore 🇸🇬
